const axios = require('axios');
const fs = require('fs');
const json2csv = require('json2csv');

// Smart contract address
const contractAddress = "Nft address";

// Wallet address to retrieve the transaction history for
const accountAddress = "contract address";

const apiKey = "polygon api";
const urlAPI="https://api-testnet.polygonscan.com/api?module=account&action=txlist&address=//{contract address}&startblock=0&endblock=99999999&page=1&offset=10&sort=asc&apikey=YourApiKeyToken1";

const options = {fields: ['transactionHash', 'args._from', 'args._to', 'args._value', 'timestamp']};


axios.get(urlAPI, {
    params: {
        from: accountAddress,
        apikey: apiKey
    }
}).then(response => {
    if (!response || !response.data) {
        console.log("Error: No data received from the API");
        return;
    }
    const data = response.data;
    const csv = json2csv.parse(data, options);
    fs.writeFileSync('./transactions.csv', csv);
    console.log('File saved successfully!');
}).catch(error => {
    console.log(`Error: ${error}`);
});


